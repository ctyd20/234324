<?function sortty($a, $b) {
    if ($a['SORT'] == $b['SORT']) return 0;
    return ($a['SORT'] > $b['SORT']) ? 1 : -1;
}

usort($arResult['JS_DATA']['ORDER_PROP']['properties'], 'sortty');
// OR


class FieldSorter {
    public $field;

    function __construct($field) {
        $this->field = $field;
    }

    function cmp($a, $b) {
        if ($a[$this->field] == $b[$this->field]) return 0;
        return ($a[$this->field] > $b[$this->field]) ? 1 : -1;
    }
}

$sorter = new FieldSorter('price');    
usort($array, array($sorter, "cmp"));

